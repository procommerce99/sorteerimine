<?php
  include "backend/config.php";
?>


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <title>Sorteeri</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/style.css">
    <script src="./assets/js/core/jquery.min.js"></script>
    <link rel="stylesheet" href="./assets/css/jquery-ui.min.css">
    <script src="./assets/js/core/jquery-ui.min.js"></script>
    <script>
        $(function() {
            $("#input").autocomplete({
                source: "backend/search.php",
            }); 
        });
    </script>
</head>
<body class="text-center">
  <div class="nav">
        <a href="scan/"><h6>Skänneerimine</h6></a>
  </div>
    <form class="form-sort" method="GET" action="">
      <h1 class="h3 mb-3 font-weight-normal">Sorteeri</h1>
      <input type="text" id="input" name="input" placeholder="Sisesta jäätme nimetus..." class="form-control" autocomplete="off">
      <input id="submitbtn" type="submit" name="search" value="Otsi">
            <?php
                if(isset($_GET['search'])){
                  $item_name = $_GET['input'];
                  $ean = $_GET['input'];
                  $query = "SELECT * FROM item INNER JOIN item_group ON(item_group.group_id = item.item_group_id) WHERE item_name = '$item_name' OR ean='$ean'";
                  $result = mysqli_query($db, $query);
                  if(!$result){
                    echo "Error" . $db->error;
                  }
                  
                  while($row = mysqli_fetch_array($result)){
            ?>
            
                  <form class="display" action="" method="POST">
                    <div class="card mb-4 my-card">
                    <div class="card-body">
                      <p class="my-0 font-weight-normal"> <?php echo $row['item_name']?></p>
                      </div>
                      <div class="images">
                      <?php
                        if($row['item_group_id'] == "1"){
                          echo '<img class="img-fluid" id="display-img" src="./assets/img/brown.png" alt="">';
                          $colour = "background-color:#af5f00";
                        }else if($row['item_group_id'] == "4"){
                          echo '<img class="img-fluid" id="display-img" src="./assets/img/yellow.png" alt="">';
                          $colour = "background-color:#eec403";
                        }else if($row['item_group_id'] == "5"){
                          echo '<img class="img-fluid" id="display-img" src="./assets/img/blue.png" alt="">';
                          $colour = "background-color:#0162bd";
                        }else if($row['item_group_id'] == "2"){
                          echo '<img class="img-fluid" id="display-img" src="./assets/img/grey.png" alt="">';
                          $colour = "background-color:#8f8f8f";                
                        }
                        else if($row['item_group_id'] == "3"){
                          echo '<img class="img-fluid" id="display-img" src="./assets/img/green.png" alt="">';
                          $colour = "background-color:#4ea252";
                        }
                      ?>
                      </div>
                      <div style="<?php echo $colour;?>" class="card-footer my-card-footer">
                      <h3 class="card-title pricing-card-title"> <?php echo $row['group_name'] ?></h3>
                      <p class="errormsg"><?php
                      
                      echo $error;
                    ?></p>
                  </form>
                <?php
                    }
                }

              ?>
            </ul>
          </div>
        </div>  
          </div>
        </div>
    </form>
  </body>
</html>

<?php


?>
