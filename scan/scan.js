window
  .addEventListener("load", function () {
    let selectedDeviceId;

    const codeReader = new ZXing.BrowserMultiFormatReader();
    codeReader.listVideoInputDevices().then((videoInputDevices) => {
      const sourceSelect = document.getElementById("sourceSelect");
      selectedDeviceId = videoInputDevices[0].deviceId;
      if (videoInputDevices.length >= 1) {
        videoInputDevices.forEach((element) => {
          const sourceOption = document.createElement("option");
          sourceOption.text = element.label;
          sourceOption.value = element.deviceId;
          sourceSelect.appendChild(sourceOption);
        });

        sourceSelect.onchange = () => {
          selectedDeviceId = sourceSelect.value;
        };

        const sourceSelectPanel = document.getElementById("sourceSelectPanel");
        sourceSelectPanel.style.display = "block";
      }

      codeReader.decodeFromVideoDevice(undefined, "video", (result, err) => {
        if (result) {
          var beep = new Audio("scanbeep.wav");
          beep.play();
          document.getElementById("result").value = result;
          document.getElementById("resultsubmitbtn").click();
        }
        if (err && !(err instanceof ZXing.NotFoundException)) {
          console.error(err);
          document.getElementById("result").textContent = err;
        }
      });
      console.log(
        `Started continous decode from camera with id ${selectedDeviceId}`
      );
    });
    document.getElementById("startButton").addEventListener("click", () => {
      codeReader.decodeFromVideoDevice(undefined, "video", (result, err) => {
        if (result) {
          var beep = new Audio("scanbeep.wav");
          beep.play();
          document.getElementById("result").value = result;
          document.getElementById("resultsubmitbtn").click();
        }
        if (err && !(err instanceof ZXing.NotFoundException)) {
          console.error(err);
          document.getElementById("warn").textContent = err;
        }
      });
    });
    document.getElementById("resetButton").addEventListener("click", () => {
      codeReader.reset();
    });
  })

  .catch((err) => {
    console.error(err);
  });

function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

  // Windows Phone must come first because its UA also contains "Android"
  if (/windows phone/i.test(userAgent)) {
    document.getElementById("warn2").textContent = "Windows Phone";
  }

  if (/android/i.test(userAgent)) {
    document.getElementById("warn2").textContent = "Android";
  }

  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
    document.getElementById("warn2").textContent = "iOS";
  }
}
