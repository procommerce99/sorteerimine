<?php
include "../backend/config.php";
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes" />


    <title>Skänneerimine</title>
    <?php
    include "../admin/includes/header.php";
    ?>
   <link rel="stylesheet" href="../assets/css/material-dashboard.css">
   <link rel="stylesheet" href="../assets/css/jquery-ui.min.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

</head>
<body onload=getMobileOperatingSystem()>
    <nav class="navbar navbar-light bg-light scan-navbar">
        <div class="container nav-container" style=""> 
                <div class="back-col" >
                    <a href="../index.php"><i class="material-icons" style="">navigate_before</i></a>
                </div>
                <div class="title-col" style=""> 
                    <h3>Skänneerimine</h3>
            </div>
            <div class="settings-col"><a href="#" style="text-decoration: none; " class="material-icons settings-icon" id="test" data-toggle="modal" data-target="#settingsModal">settings</a></div>
            </div>
        </div>
        </nav>  
        <div class="container scan-container">
            <span id="warn"></span>
            <span id="warn2"></span>
                <!-- Modal -->
            <div id="settingsModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Sätted</h4>
                </div>
                <span id="warn2"></span>

                <div class="modal-body">
               <i class="material-icons">help</i> <small class="info-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic, perferendis!</small>

                <div id="sourceSelectPanel" style="display:none">
                    <label for="sourceSelect">Vali kaamera</label><br>
                    <select id="sourceSelect" style="max-width:400px">
                </select>
                </div>
                <a class="btn btn-info" id="resetButton">Lähtesta</a>
                <a class="btn btn-primary" id="startButton">Alusta</a>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Sulge</button>
                </div>
                </div>
            </div>
            </div>
            <div>
                <video id="video"  style="width: 100%;">Sinu brauser ei toeta video kuvamist.</video>
            </div>
            <form action="scan.php" method="post" target="frame" id="result-form">
                <input type="text" id="result" name="result" hidden>
                <input type="submit" name="submit" value="Submit" id="resultsubmitbtn">
                <button onclick="()">Ter</button>
            </form>
        </div>
        <iframe name="frame"></iframe>
    </div>
    <script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script>
    <script src="scan.js" type="text/javascript"></script> 
    <script src="../assets/js/core/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</body>

</html>
