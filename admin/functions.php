<?php
    // Search (index.php)
    if(isset($_GET['search'])){
        $item_name = $_GET['input'];
        $query = "SELECT * FROM item INNER JOIN item_group ON(item_group.group_id = item.item_group_id) WHERE item_name = '$item_name'";
        $result = mysqli_query($db, $query);
        if(!$result){
          echo "Error" . $db->error;
        }
    }

    // Add items (additem.php)
    if(isset($_POST['additem'])){
        
        $item_name = $_POST['item_name'];
        $item_ean = $_POST['item_ean'];
        $item_group_id = $_POST['item_group_id'];
        $item_status = $_POST['item_status'];

        $stmt = $db->prepare("INSERT INTO item(item_name, `status`, ean, item_group_id) VALUES(?, ?, ?, ?)");
        $stmt->bind_param('siii', $item_name, $item_status, $item_ean, $item_group_id);
        if($stmt->execute()){
            include "./includes/alerts/successadd.php";
        }else{
            $error = "Error: " . $stmt->error;
        }
    }


    // Display edit items (edititem.php)
    if(isset($_GET['edit'])){
        $id = $_GET['edit'];
        $query = $db->query("SELECT id, item_name, `status`, ean, group_name FROM item INNER JOIN item_group ON(item_group.group_id = item.item_group_id) WHERE id = $id");
        if($query){
            $row = $query->fetch_array();
            $item_name = $row['item_name'];
            $item_ean = $row['ean'];
            $group_name = $row['group_name'];
            $item_status = $row['status'];
        }else{
            $error = "Error: " . $query->error;
        }
        
    }

    // Edit items (edititem.php)     
    if(isset($_POST['edit'])){
        $id = $_POST['id'];
        $item_name = $_POST['item_name'];
        $item_ean = $_POST['item_ean'];
        $group_name = $_POST['group_name'];
        $item_status = $_POST['item_status'];
        $stmt = $db->prepare("UPDATE item SET item_name=?, `status`=?, ean=?, item_group_id=?, edited_at=CURRENT_TIMESTAMP WHERE id=$id");
        $stmt->bind_param('siii', $item_name, $item_status, $item_ean, $group_name);
        $stmt->execute(); 
        if($stmt->execute()){
            include "./includes/alerts/successedit.php";
        }else{
            $error = "Error: " . $stmt->error;
        }
    }


?>
