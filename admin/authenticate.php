<?php
session_start();

include "../backend/config.php";

if ( !isset($_POST['username'], $_POST['password']) ) {
    exit('Palun täida mõlemad väljad');
} 
if($stmt = $db->prepare('SELECT id, password FROM account WHERE username = ?')) {
    $stmt->bind_param('s', $_POST['username']);
    $stmt->execute();
    $stmt->store_result();
    if ($stmt->num_rows > 0) {
        $stmt->bind_result($id, $password);
        $stmt->fetch();
        if (password_verify($_POST['password'], $password)) {
            session_regenerate_id();
            $_SESSION['loggedin'] = TRUE;
            $_SESSION['name'] = $_POST['username'];
            $_SESSION['id'] = $id;
            header('Location: products.php');
        } else {
            echo 'Vale parool!';
        }
    } else {
        echo 'Vale kasutajanimi!';
    }
    $stmt->close();
    }
?>
