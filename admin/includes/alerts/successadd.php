<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    swal({
        title: "Õnnestus!",
        text: "Andmed edukalt lisatud.",
        type: "success",
    }).then(function() {
        window.location.href = "products.php";
    })
</script>