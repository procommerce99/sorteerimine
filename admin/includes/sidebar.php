<div class="wrapper ">
    <div class="sidebar" data-color="rose" data-background-color="black" data-image="../assets/img/sidebar-1.jpg">
      <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          CT
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
          Creative Tim
        </a>
      </div>
      <div class="sidebar-wrapper">
        <div class="user">
          <div class="photo">
            <img src="../assets/img/faces/avatar.jpg" />
          </div>
          <div class="user-info">
            <a data-toggle="collapse" href="#collapseExample" class="username">
              <span>
              <?php
                  echo $_SESSION['name'];
                ?>
                <b class="caret"></b>
              </span>
            </a>
            <div class="collapse" id="collapseExample">
              <ul class="nav">
                <li class="nav-item">
                  <a class="nav-link" href="logout.php">
                    <span class="sidebar-mini"> L </span>
                    <span class="sidebar-normal"> Logi välja </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <ul class="nav">
          <!-- <li class="nav-item">
            <a class="nav-link" href="../dashboard/index.php">
            <i class="material-icons">dashboard</i>
            <p> Dashboard </p>
          </a>
        </li> -->
        <li class="nav-item">
              <a class="nav-link" href="../admin/products.php">
                <i class="material-icons">assignment</i>
                <p>Tooted</p>    
              </a> 
            </li>
        
        </ul>
      </div>
    </div>